# snippets

## Description
This directory contains SVG snippets - small SVG files representing small modules, parts of which may be incoprorated into larger SVG files with minimal changes being made e.g. _jogshuttle_ assembly which may be used in multiple different devices.

## Usage
Typically, the inner <svg> section of a snippet can be copied as is into another SVG  files, changing only its _x=_ and _y=_ definitions to locate it within the larger SVG file. For example, from the _jogshuttle_ snippet, copying the lines:
```
    <svg id="jog_shuttle_1" x="0" y="300" width="400" height="300" viewBox="0 0 400 300" >
      <rect class="deck" x="0" y="0" height="300" id="jog_deck" width="400"/>
      <circle class="shuttle" cx="200" cy="150" id="shuttle_1" r="141"/>
      <circle class="jog" cx="200" cy="150"  id="jog_1" r="120" />
      <circle cx="265" cy="207" fill="none" id="jog_1_1" r="25" stroke="#000000"/>
    </svg>
```
into another SVG file would place a _jogshuttle_ assembly at position 0,300 of the larger SVG file. Edit _x=0_ and _y=300_ to position the _jogshuttle_ assembly in the desired position. Note also the use of _class_ definitions of each snippet; those used in the snippet (_deck_, _shuttle_ & _jog_ in this example) will also need to be copied into the larger enclosing SVG file.


## Support
Any problems, comments, suggestions are welcome via the [issues page](https://gitlab.com/chris.willing.oss/xkeys-layouts/-/issues).

## Acknowledgments
Thanks to [P.I. Engineering](https://xkeys.com) for providing initial SVG files.

## License
MIT

