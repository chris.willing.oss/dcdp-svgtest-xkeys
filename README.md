# xkeys-layouts


## Description
This project stores [SVG](https://www.w3.org/Graphics/SVG/) representations of [X-keys devices](https://xkeys.com) manufactured by P.I. Engineering.

Although designed as an [NPM module](https://www.npmjs.com/package/xkeys-layouts), other frameworks may download the layout data directly from this repository as required. The <i>products</i> directory has subdirectories for each X-keys device, each of which contains the available SVG data.

## Status
This project is in an experimental phase while testing its suitability for integration with other projects. As a result, the device folders are not yet fully poulated.

## Installation
To install as an NPM module, run:
```
    npm install --save xkeys-layouts
```

## Usage
```
    xkeys_layouts = require('xkeys-layouts');
```
`summary` commands return a listing of available data as an object formated as:
```
    {
      DEVNAM1: [file1, file2, ...],
      DEVNAM2: [file1, file2, ...],
      ...
    }
```
where file1, file2, ... are filenames only.

```
    // Retrieve summary of all devices
    xkeys_layouts.summary();

    // Retrieve summary of XK8 device
    xkeys_layouts.summary("XK8");

    // Retrieve summaries of XK8 and XK12JOG devices
    xkeys_layouts.summary("XK8", "XK12JOG");
```

`fetch` commands return actual device SVG data as an object formated as:
```
    {
      DEVNAM1: {file1:"file1_data", file2:"file2_data, ..., fileN:"fileN_data"},
      DEVNAM2: {file1:"file1_data", file2:"file2_data, ..., fileN:"fileN_data"},
      ...
    }
```
where file1_data, file2_data, ...  are nodejs Buffers resulting from [reading the data files](https://nodejs.org/api/fs.html#fsreadfilesyncpath-options).
```
    // Fetch SVG data for XK8, XKE40 and XK12JOG devices
    xkeys_layouts.fetch("XK8","XKE40","XK12JOG");

```

## Support
Any problems, comments, suggestions are welcome via the [issues page](https://gitlab.com/chris.willing.oss/xkeys-layouts/-/issues).

## Acknowledgments
Thanks to [P.I. Engineering](https://xkeys.com) for providing initial SVG files.

## License
MIT

